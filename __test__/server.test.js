const request = require('supertest')

const server = require('../src/server')

describe('Deve responder na raiz', () => {
  it('na path /', () => {
    return request(server)
      .get('/')
      .then(res => expect(res.status).toBe(200))
  })
  it('na path /tes', () => {
    return request(server)
      .get('/tes')
      .then(res => expect(res.status).toBe(404))
  })
})
