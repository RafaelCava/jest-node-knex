describe('Devo conhecer as principais assertivas do jest', () => {
  it('teste do number', () => {
    let number = null
    expect(number).toBeNull()
    number = 10
    expect(number).not.toBeNull()
    expect(number).toBe(10)
    expect(number).toEqual(10)
    expect(number).toBeGreaterThan(9)
    expect(number).toBeLessThan(11)
  })
})

describe('Devo saber trabalhar com objetos', () => {
  const obj = {
    name: 'john',
    email: 'john@email.com'
  }
  const obj2 = {
    name: 'john',
    email: 'john@email.com'
  }
  it('objetos nome', () => {
    expect(obj).toHaveProperty('name')
  })
  it('objetos email', () => {
    expect(obj).toHaveProperty('email')
  })
  it('objeto igual objeto', () => {
    expect(obj).toEqual(obj2)
  })
})
