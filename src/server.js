const express = require('express')
const app = express()
const { PrismaClient } = require('@prisma/client')

app.use(express.json())

const prisma = new PrismaClient()

app.get('/', async (req, res) => {
  await prisma.wallet
    .findMany({
      include: {
        User: true
      }
    })
    .then(wallets => {
      return res.status(200).json({ wallets })
    })
    .catch(e => {
      throw new Error(e.message)
    })
    .finally(async () => {
      await prisma.$disconnect()
    })
})
app.get('/users', async (req, res) => {
  await prisma.user
    .findMany({
      include: {
        wallets: true
      }
    })
    .then(users => {
      return res.status(200).json({ users })
    })
    .catch(e => {
      throw new Error(e.message)
    })
    .finally(async () => {
      await prisma.$disconnect()
    })
})

app.post('/', async (req, res) => {
  const { name, email, password, idade, wallet } = req.body
  const data = wallet
    ? {
        name,
        email,
        password,
        idade,
        wallets: {
          create: {
            name: wallet
          }
        }
      }
    : {
        name,
        email,
        password,
        idade
      }
  await prisma.user
    .create({
      data
    })
    .then(user => {
      return res.status(200).json({ user })
    })
    .catch(e => {
      throw new Error(e.message)
    })
    .finally(async () => {
      await prisma.$disconnect()
    })
})

module.exports = app
