<div align="center" id="top"> 
  <img style="width: 100%; max-width: 200px; border-radius: 20px;" src="./public/logo.jpg" alt="Jest Node Knex" />

  &#xa0;

  <!-- <a href="https://jestnodeknex.netlify.com">Demo</a> -->
</div>

<h1 align="center">Jest Node Knex</h1>

<p align="center">
  <img alt="Principal linguagem do projeto" src="https://img.shields.io/github/languages/top/RafaelCava/jest-node-knex?color=56BEB8">

  <img alt="Quantidade de linguagens utilizadas" src="https://img.shields.io/github/languages/count/RafaelCava/jest-node-knex?color=56BEB8">

  <img alt="Tamanho do repositório" src="https://img.shields.io/github/repo-size/RafaelCava/jest-node-knex?color=56BEB8">

  <img alt="Licença" src="https://img.shields.io/github/license/RafaelCava/jest-node-knex?color=56BEB8">

  <!-- <img alt="Github issues" src="https://img.shields.io/github/issues/RafaelCava/jest-node-knex?color=56BEB8" /> -->

  <!-- <img alt="Github forks" src="https://img.shields.io/github/forks/RafaelCava/jest-node-knex?color=56BEB8" /> -->

  <!-- <img alt="Github stars" src="https://img.shields.io/github/stars/RafaelCava/jest-node-knex?color=56BEB8" /> -->
</p>

<!-- Status -->

<h4 align="center"> 
	🚧  Jest Node Knex 🚀 Em construção...  🚧
</h4> 

<hr>

<p align="center">
  <a href="#dart-sobre">Sobre</a> &#xa0; | &#xa0; 
  <a href="#sparkles-funcionalidades">Funcionalidades</a> &#xa0; | &#xa0;
  <a href="#rocket-tecnologias">Tecnologias</a> &#xa0; | &#xa0;
  <a href="#white_check_mark-pré-requisitos">Pré requisitos</a> &#xa0; | &#xa0;
  <a href="#checkered_flag-começando">Começando</a> &#xa0; | &#xa0;
  <a href="#memo-licença">Licença</a> &#xa0; | &#xa0;
  <a href="https://github.com/RafaelCava" target="_blank">Autor</a>
</p>

<br>

## :dart: Sobre ##

Projeto criado para estudo para melhorar as habilidades com testes

## :sparkles: Funcionalidades ##

:heavy_check_mark: Gera um servidor na porta 3001\
:heavy_check_mark: Realiza diversas requisições http\

## :rocket: Tecnologias ##

As seguintes ferramentas foram usadas na construção do projeto:

- [Node.js](https://nodejs.org/en/)
- [Express.js](https://expressjs.com/pt-br/)
- [Docker](https://www.docker.com/)

## :white_check_mark: Pré requisitos ##

Antes de começar :checkered_flag:, você precisa ter o [Git](https://git-scm.com) e o [Node](https://nodejs.org/en/) ou [Docker](https://www.docker.com/) instalados em sua maquina.

## :checkered_flag: Começando ##

```bash
# Clone este repositório
$ git clone https://github.com/RafaelCava/jest-node-knex

# Entre na pasta
$ cd jest-node-knex

# Sem Docker
# Instale as dependências
$ yarn

# Para iniciar o projeto
$ yarn start

# Com Docker
# Suba os containers
$ docker-compose up -d

# O app vai inicializar em <http://localhost:3000>
```

## :memo: Licença ##

Este projeto está sob licença MIT. Veja o arquivo [LICENSE](LICENSE.md) para mais detalhes.


Feito com :heart: por <a href="https://github.com/RafaelCava" target="_blank">RafaelCava</a>

&#xa0;

<a href="#top">Voltar para o topo</a>
